const express= require('express');
const cors=require('cors');
var bodyParser = require('body-parser');

const app =express()
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
const MongoClient=require('mongodb').MongoClient;


var port=process.env.PORT || 8080;
app.use(cors());
app.use(express.static('/'))
app.listen(port);
console.log("Express starting on port:"+port);
const mongourl = "mongodb+srv://cca-uppalas4:qwerty123@cluster0.xn4ns.mongodb.net/cca-labs?retryWrites=true&w=majority"

    const dbClient = new MongoClient(mongourl,{useNewUrlParser:true,useUnifiedTopology:true});
    dbClient.connect((err)=>{
        if(err) throw err;
        console.log("Connected to the MongoDB cluster");
    });
app.get('/',(req,res)=>{
    res.sendFile(__dirname+'/index.html'); 
});

app.post('/login', function (req, res) {
    let username=req.body.username
    let password=req.body.password
    const db =dbClient.db();
    db.collection("users").findOne({username:username}).then(results=>{
        console.log(results);
        if(results && results['username']==username && results['password']==password){
            res.json({ code:200,message: 'login successful',username:results["username"],fullname:results["fullname"]})
        }else{
            res.json({code:400,message:"Invalid credentials"})
        }
    });
    
        
})
app.post('/signup',function (req, res){
        console.log("body of request")
        console.log(req.body);
        let username=req.body.username;
        const db =dbClient.db();
        db.collection("users").findOne({username:username})
            .then(results=>{
                console.log(results);
                if(results){
                    results.json({ code:400,message: 'User Already Exists' })
                }else{
                    let data={
                        "username":req.body.username,
                        "password":req.body.password,
                        "fullname":req.body.fullName,
                        "email":req.body.email
                    }
                    db.collection("users").insertOne(data).then(user=>{
                        console.log(user)
                        res.json({ code:200,message: 'Signup Successful! You Can Login Now' })
                    }).catch(err=>{
                        res.json({ code:400,message: err })
                    });
                }
            })
            .catch(err=>{
                res.json({ code:504,message: err });
            });

})
app.post('/newMessage',function(req,res){
    let sender=req.body.sender
    let receiver= req.body.receiver
    let msg=req.body.msg
    const db =dbClient.db();
    let data={
        "sender":req.body.sender,
        "receiver":req.body.receiver,
        "message":req.body.msg
    }
    db.collection("chatMessages").insertOne(data).then(res=>{
        res.json({ code:200,message: 'message sent successfully' })
    }).catch(err=>{
        res.json({ code: 400,err});
    })
})
app.post('/saveMessage', function (req, res) {
    console.log(req.body)
    let sender =req.body.sender
    let receiver=req.body.receiver
    let data={
        sender :req.body.sender,
        receiver:req.body.receiver,
        message:req.body.message,
        timestamp:Date.now()
    }
    if(data.message==null){
        data.message='';
    }
    const db =dbClient.db();
    db.collection("chat").insertOne(data).then(results=>{
        console.log(results);
        res.json({
            "code":200,
            "msg":"Inserted Successfully"
        })
    }).catch(err=>{
        console.log(err);
        res.json({
            "code":500,
            "msg":"Message is not inserted"
        })
    }); 
})

